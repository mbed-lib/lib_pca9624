/*
 * PCA9624.h
 * Copyright (C) 2022 Adrian Badoil 
 *
 * Distributed under terms of the MIT license.
 */

#include "mbed.h"

#ifndef PCA9624_H
#define PCA9624_H

// All register addresses assume IOCON.BANK = 0 (POR default)
#define MODE1      0x00
#define MODE2      0x01
#define PWM0       0x02
#define PWM1       0x03
#define PWM2       0x04
#define PWM3       0x05
#define PWM4       0x06
#define PWM5       0x07
#define PWM6       0x08
#define PWM7       0x09
#define GRPPWM     0x0a
#define GRPFREQ    0x0b
#define LEDOUT0    0x0c
#define LEDOUT1    0x0d
#define SUBADR1    0x0e
#define SUBADR2    0x0f
#define SUBADR3    0x10
#define ALLCALLADR 0x11

#define DISABLE            0x00
#define FULLY_ON           0x01
#define PWM_EACH           0x02
#define PWM_EACH_AND_GROUP 0x03

class PCA9624 {
public:
    /** Create an PCA9624 object connected to the specified I2C object and using the specified deviceAddress
    *
    * @param I2C &i2c the I2C port to connect to
    * @param char deviceAddress the address of the MCP23017
    */
    PCA9624(I2C *i2c, char deviceAddress, const unsigned int p_frequency = 100000);
    
    //bool setDriverOutputState(uint8_t state);
        /** Init PCA9624
    *
    * @param
    * @returns true on success, false otherwise
    */
    bool init();
    bool drive(const char ch, const char vol);
    bool drive_4(const char vol_1, const char vol_2, const char vol_3, const char vol_4);
    bool sleep(const bool b);
    bool setGroupPWM(const char vol);
    bool reset();

protected:
    I2C *_i2c;
    char _readOpcode;
    char _writeOpcode;

    /** Init PCA9624
    *
    * @param
    * @returns true on success, false otherwise
    */
    bool _init();

    /** Write to specified PCA9624 register
    *
    * @param char address the internal registeraddress of the PCA9624
    * @returns true on success, false otherwise
    */
    bool _write(char address, char byte);
    bool _write_4(char address, char byte_1, char byte_2, char byte_3, char byte_4);

    /** Read from specified PCA9624 register
    *
    * @param char address the internal registeraddress of the PCA9624
    * @returns data from register
    */
    char _read(char address);
};

#endif // PCA9624_H
